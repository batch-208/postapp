package com.b208.postApp.models;

import javax.persistence.*;

//Mark this Java object as a representation of a database table via the use of @Entity
@Entity
//Designate the table name that this model is connected to:
@Table(name="posts")
public class Post {
    //indicates that the following property is a primary key via @id
    @Id
    //Auto-increment the id property using @GeneratedValue
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //Class properties that represent table columns in a relational database as @Columns
    @Column
    private String title;

    @Column
    private String content;

    //We represent and tell Hibernate that this class represents that many side of the relationship
    //And that each post is linked to a user via @JoinColumn, a reference using a foreign key to a user primary key
    //This column is also mapped by our user, that when the getPost() method of the User is called, all post rows, data belonging to the use will be gathered and returned
    //user_id will be the name of the foreign_key column
    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    //Constructors and Setters/Getters

    //default constructor is needed when retrieving our posts
    public Post(){}

    //parameterized constructor needed when creating posts
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

