package com.b208.postApp.controllers;


import com.b208.postApp.exceptions.UserException;
import com.b208.postApp.models.User;
import com.b208.postApp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userServices;

    //Academic Reading List
    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody User user){

        if(userServices.createUser(user) == true){
            return new ResponseEntity<>("Please add an 8 or more character password", HttpStatus.BAD_REQUEST);
        } else {
            userServices.createUser(user);
            return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
        }
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userServices.getUser(),HttpStatus.OK);
    }

    @PostMapping("/users/register")
    public ResponseEntity<Object> register(@RequestBody Map<String,String> body) throws UserException {
        String username = body.get("username");

        if(!userServices.findByUsername(username).isEmpty()){
            throw new UserException("Username already exists");
        }else {
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            User newUser = new User(username,encodedPassword);
            userServices.createUser(newUser);

            return new ResponseEntity<>("User registered successfully",HttpStatus.CREATED);
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id,@RequestBody User user){
        userServices.updateUser(id,user);
        return new ResponseEntity<>("User Updated Successfully",HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){
        userServices.deleteUser(id);
        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
    }

    //Academic Reading List
//    @GetMapping("/users/{id}")
//    public ResponseEntity<Object> searchUser(@PathVariable Long id){
//        userServices.searchUser(id);
//        return new ResponseEntity<>(userServices.searchUser(id),HttpStatus.OK);
//    }
//
//    @GetMapping("/users/check/{id}")
//    public ResponseEntity<Object> checkUser(@PathVariable Long id){
//        userServices.checkUser(id);
//        if(userServices.checkUser(id) == true){
//            return new ResponseEntity<>("User has been found", HttpStatus.FOUND);
//        } else {
//        }
//        return new ResponseEntity<>("No user found",HttpStatus.NOT_FOUND);
//    }
//
//    @GetMapping("/users/count")
//    public ResponseEntity<Object> countUser(){
//        userServices.countUser();
//        return new ResponseEntity<>(userServices.countUser(),HttpStatus.OK);
//    }
}

