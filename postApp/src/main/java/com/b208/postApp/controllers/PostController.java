package com.b208.postApp.controllers;

import com.b208.postApp.models.Post;
import com.b208.postApp.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//Handle all HTTP responses
@RestController
//Enables cross origin resource requests
//CORS - cross origin resource sharing is the ability to allow or disallow applications to share resources from each other. With this, we can allow or disallow clients to access out API
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //map web requests to controller methods via @RequestMapping
    //@RequestMapping(value = "/posts", method = RequestMethod.POST)
    //@PostMapping maps post method requests into an endpoint
    @PostMapping("/posts")
    //@ResponseEntity is an object that represents and contains the whole HTTP response, the actual message, and the status code.
    //@RequestBody automatically converts JSON client input into our desired object
    //@RequestHeader(value="Authorization") allows us to get the value passed as Authorization from our request
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
        //@RequestBody converts the JSON input as the declared class of the parameter
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());

        //Use the createPost() method from our service and pass our post object
        //Academic Reading List
        if(postService.createPost(stringToken,post) == true){
            return new ResponseEntity<>("Invalid input, please complete the form",HttpStatus.BAD_REQUEST);
        } else {
            //postService.createPost(stringToken,post);
            return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
        }

        /*
        Request Body (JSON) -> Post post (Post class object) in the controller -> postService.createPost(post) -> postRepository.save(post)
        */

    }

    @GetMapping("/posts")
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<Object> updatePost(@RequestHeader(value="Authorization") String stringToken, @PathVariable Long id, @RequestBody Post post){
        //System.out.println(id);
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());
        //Pass the id and the request body as post to our postService method

        return postService.updatePost(stringToken,id,post);
    }

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Object> deletePost(@RequestHeader(value = "Authorization") String stringToken, @PathVariable Long id){
        //postService.deletePost(stringToken,id);

        return postService.deletePost(stringToken,id);
    }

    @GetMapping("/posts/myPosts")
    public Object getMyPosts(@RequestHeader(value = "Authorization") String stringToken){
        return postService.getMyPosts(stringToken);
    }
}

